# Custom hook

A custom hook useGeolocation

Initial code from:
https://www.udemy.com/course/the-ultimate-react-course/learn/lecture/37350908#questions

Based on the initial code, I created a custom hook useGeolocation.
(Two versions, w. and w/o useCallback App-v1.js and useGeolocation-v1.js are my initial version of useGeolocation hook (with useCallback), and App.js and useGeolocation.js are the second version w/o useCallback).

Click the button and see your geolocation:

![geolocation](./public/geolocation1.png)

Styles by Katlin.
