import { useCallback, useState } from "react";
//Idea from: https://dev.to/karunamaymurmu/comment/1g9kj

export function useGeolocation(customErrorMsg, customCallback) {
  const [isLoading, setIsLoading] = useState(false);
  const [position, setPosition] = useState({});
  const [error, setError] = useState(null);

  const getPosition = useCallback(() => {
    customCallback?.();
    if (!navigator.geolocation) return setError(customErrorMsg);
    setIsLoading(true);
    navigator.geolocation.getCurrentPosition(
      (pos) => {
        setPosition({
          lat: pos.coords.latitude,
          lng: pos.coords.longitude,
        });
        setIsLoading(false);
      },
      (error) => {
        setError(error.message);
        setIsLoading(false);
      }
    );
  }, [customErrorMsg, customCallback]);
  return { getPosition, isLoading, position, error };
}
